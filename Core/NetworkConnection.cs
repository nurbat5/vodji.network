﻿//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Channels;
//using Vodji.Network.Channels.Reliable;
//using Vodji.Network.Core.Interfaces;
//using Vodji.Network.Data.Interface;
//using Vodji.Network.Identity;
//using Vodji.Network.Identity.Interfaces;
//using Vodji.Network.Provider;
//using Vodji.Network.Utils;
//using Vodji.Shared.Utils;

//namespace Vodji.Network.Core
//{
//    public abstract class NetworkConnection
//    {
//        public INetworkChannelHandler   ChannelHandler    { get; }
//        public INetworkHandler           NetworkHandler    { get; }
//        public INetworkProvider         NetworkProvider   { get; }

//        protected NetworkConnection(INetworkProvider networkProvider)
//        {
//            ChannelHandler = new NetworkChannelHandler();
//            NetworkHandler = new NetworkHandler();
//            NetworkProvider = networkProvider;
//            {
//                NetworkProvider.Listen();
//            }
//            RegisterEvents();
//        }

//        // Methods
//        public void SendToAll<TMessage>(TMessage msg) where TMessage : INetworkMessage
//        {
//            var packMessage = NetworkHelper.PackMessage(msg);
//            var arraySegment = packMessage.ToArraySegment();

//            LoggingManager.GetLogger().LogTrace($"Send to all");
//            foreach (var networkIdentity in connections.KeysT2)
//            {
//                LoggingManager.GetLogger().LogTrace($"Send to all ({networkIdentity.NetworkPoint})");
//                networkIdentity.SendMessage<ReliableChannel>(arraySegment);
//            }
//        }

//        public void SendToServer<TMessage>(TMessage msg) where TMessage : INetworkMessage
//        {
//            //var packMessage = NetworkHelper.PackMessage(msg);
//            //var arraySegment = packMessage.ToArraySegment();

//            //networkOptions.NetworkProvider.Send(arraySegment);
//        }


//        protected virtual void RegisterEvents()
//        {
//            NetworkProvider.EventClientConnected.AddListener(OnClientConnected);
//            NetworkProvider.EventClientDisconnected.AddListener(OnClientDisconnected);
//            NetworkProvider.EventDataReceived.AddListener(OnRecivedData);
//            NetworkProvider.EventDataSend.AddListener(OnSendingData);
//        }
//        protected virtual void OnSendingData(INetworkPoint networkPoint, int sentNumBytes)
//        {
//        }

//        protected virtual void OnRecivedData(INetworkPoint networkPoint, byte[] bytes)
//        {
//            if(connections.TryGetValue(networkPoint, out var networkIdentity))
//            {
//                LoggingManager.GetLogger().LogInformation($"Data recived {bytes.Length} bytes from {networkPoint}");
//                networkIdentity.OnInMessage(bytes);
//                return;
//            }

//            LoggingManager.GetLogger().LogWarning("Unknown connectionId: " + networkPoint);
//        }
//        protected virtual void OnClientDisconnected(INetworkPoint networkPoint)
//        {
//            if(!connections.Contains(networkPoint))
//            {
//                LoggingManager.GetLogger().LogInformation($"[NOT USER] Disconnect: {networkPoint.ID}");
//                return;
//            }

//            connections[networkPoint].Disconnect();
//            connections.Remove(networkPoint);
//            LoggingManager.GetLogger().LogInformation($"Disconnect: {networkPoint.ID}");
//        }
//        protected virtual void OnClientConnected(INetworkPoint networkPoint)
//        {
//            if(connections.Contains(networkPoint))
//            {
//                LoggingManager.GetLogger().LogInformation($"Server connectionId {networkPoint.ID} already in use.");
//                return;
//            }

//            if(connections.Count > 20)
//            {
//                LoggingManager.GetLogger().LogInformation($"Server full. Kicked {networkPoint.ID}");
//                return;
//            }

//            LoggingManager.GetLogger().LogInformation($"Server accepted: {networkPoint.ID}");
//            var networkIdentity = new NetworkIdentity(networkPoint);
//            {
//                networkIdentity.ChannelHandler = new NetworkChannelHandler(networkIdentity, ChannelHandler);
//                networkIdentity.NetworkHandler = NetworkHandler;
//            };
//            connections.Add(networkPoint, networkIdentity);
//        }

//        protected DictionaryPair<INetworkPoint, INetworkIdentity> connections = new DictionaryPair<INetworkPoint, INetworkIdentity>();
//    }
//}
