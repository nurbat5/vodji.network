﻿//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Channels.Interfaces;
//using Vodji.Network.Core.Interfaces;
//using Vodji.Network.Data;
//using Vodji.Network.Data.Interface;
//using Vodji.Network.Identity.Interfaces;
//using Vodji.Shared.Background;

//namespace Vodji.Network.Channels.Reliable
//{
//    public class ReliableChannel : TaskApplication, INetworkChannel
//    {
//        public float RetransmitTimeout { get; set; } = 300;
//        public int RetransmitRetry { get; set; } = 5;
//        public int TailLoss { get; set; } = 5;
//        public int WindowSize { get; set; } = 256;

//        public ReliableChannel()
//        {
//            cachedMessages = new Dictionary<int, ReliableChannelPacket>();
//            queuePackets = new Queue<ReliableChannelPacket>();
//        }

//        public INetworkChannelOptions ChannelOptions { get; set; }

//        #region Recived

//        public void OnInMessage(INetworkReader networkReader, INetworkIdentity networkIdentity)
//        {
//            var reliablePacket = new ReliableChannelPacket();
//            {
//                (reliablePacket as INetworkMessage).Deserialize(networkReader);
//            }
//        }

//        #endregion

//        #region Sending
//        public void OnOutMessage(INetworkChannelPacket channelPacket, INetworkIdentity networkIdentity)
//        {
//            if(queuePackets.Count > WindowSize)
//            {
//                LoggingManager.GetLogger().LogWarning("Full buffer");
//                return;
//            }

//            this.networkIdentity ??= networkIdentity;
//            var reliablePacket = new ReliableChannelPacket
//            {
//                _identity = networkIdentity,
//                _channel = channelPacket.Channel,
//                Payload = channelPacket.Payload
//            };

//            queuePackets.Enqueue(reliablePacket);
//            LoggingManager.GetLogger().LogWarning("Send to Enqueue");
//        }

//        public override Task OnUpdateAsync()
//        {
//            // Отправка писемв очередях
//            var maxPackets = WindowSize - cachedMessages.Count;
//            lock(cachedMessages)
//            {
//                while(maxPackets != 0 && queuePackets.TryDequeue(out var reliablePacket))
//                {
//                    reliablePacket.Sequence = NextSendSequence();
//                    reliablePacket._sentTime = lastSentTime = DateTime.UtcNow;
                    
//                    var networkWriter = new NetworkWriter();
//                    networkWriter.Write(reliablePacket._channel);
//                    (reliablePacket as INetworkMessage).Serialize(networkWriter);
                    
//                    LoggingManager.GetLogger().LogTrace($"Send packet #{reliablePacket.Sequence}");
//                    //networkIdentity.NetworkPoint.SendMessage(networkWriter.ToArray());

//                    cachedMessages[reliablePacket.Sequence] = reliablePacket;
//                    maxPackets--;
//                }
//            }

//            // отправка сообщений, которые не получили ответ по таймауту
//            var time = DateTime.UtcNow;
//            foreach(var (sequence, reliablePacket) in cachedMessages)
//            {
//                if((time - reliablePacket._sentTime).TotalMilliseconds > RetransmitTimeout)
//                {
//                    LoggingManager.GetLogger().LogDebug($"Timeout #{sequence}");
//                    if(reliablePacket._retryCount > RetransmitRetry)
//                    {
//                        LoggingManager.GetLogger().LogDebug($"Too many retries #{sequence}");
//                        cachedMessages.Remove(sequence);
//                        continue;
//                    }

//                    Resend(sequence);
//                    continue;
//                }

//                if ((time - lastSentTime).TotalSeconds > TailLoss)
//                {
//                    LoggingManager.GetLogger().LogDebug($"Send lost packets #{sequence}");
//                    Resend(sequence);
//                    continue;
//                }
//            }

//            return base.OnUpdateAsync();
//        }

//        // заново отправить сообщение
//        protected void Resend(int sequenceMessage)
//        {
//            lock (cachedMessages)
//            {
//                if (cachedMessages.TryGetValue(sequenceMessage, out var reliablePacket))
//                {
//                    reliablePacket._retryCount++;
//                    reliablePacket._sentTime = lastSentTime = DateTime.UtcNow;

//                    var networkWriter = new NetworkWriter();
//                    networkWriter.Write(reliablePacket._channel);
//                    (reliablePacket as INetworkMessage).Serialize(networkWriter);

//                    LoggingManager.GetLogger().LogTrace($"Resend: {sequenceMessage}");
//                    //networkIdentity.NetworkPoint.SendMessage(networkWriter.ToArray());
//                    return;
//                }
//            }

//            LoggingManager.GetLogger().LogTrace($"Resend canceled: {sequenceMessage}");
//        }

//        protected ushort NextSendSequence() => (ushort)(sequenceSended++ % WindowSize);


//        protected Queue<ReliableChannelPacket> queuePackets;
//        protected int sequenceSended;
//        protected DateTime lastSentTime;
//        #endregion

//        protected Dictionary<int, ReliableChannelPacket> cachedMessages;
//        protected INetworkIdentity networkIdentity;
//    }
//}
