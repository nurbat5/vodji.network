﻿//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Data.Interface;
//using Vodji.Network.v2.Common;
//using Vodji.Network.v2.Components;
//using Vodji.Network.v2.Connections;
//using Vodji.Network.v2.Connections.Interfaces;
//using Vodji.Shared.Utils;

//namespace Vodji.Network.v2
//{
//    public enum NetworkConnectionStatus
//    {
//        None,
//        Connecting,
//        Connected,
//        Disconnected
//    }

//    public class NetworkClient<T> where T : INetworkConnection
//    {
//        public T Connection { get; }

//        public NetworkClient()
//        {
//            Connection = Activator.CreateInstance<T>();
//            {
//                Connection.EventClientConnected.AddListener(OnConnected);
//                Connection.EventClientDisconnected.AddListener(OnDisconnected);
//                Connection.EventRecivedDatagram.AddListener(OnDataReceived);
//            }
//        }

//        private void OnConnected(INetworkPoint networkPoint)
//        {
//            if (serverConnection is null)
//            {
//                LoggingManager.GetLogger().LogTrace($"Connection {networkPoint} connected is success.");
//                ConnectionStatus = NetworkConnectionStatus.Connected;
//                var networkIdentity = new NetworkIdentity(networkPoint);
//                {
//                    networkIdentity.SetHandler(NetworkHandler);
//                    networkIdentity.SetMiddleware(NetworkMiddlewares);
//                }

//                serverConnection = new Tuple<INetworkPoint, NetworkIdentity>(networkPoint, networkIdentity);
//                return;
//            }

//            LoggingManager.GetLogger().LogInformation("Skipped Connect message handling because connection is not null.");
//        }

//        private void OnDataReceived(INetworkPoint networkPoint, byte[] bytesRecived)
//        {
//            if (serverConnection is not null && serverConnection.Item1.Equals(networkPoint))
//            {
//                serverConnection.Item2.DatagramReceived(bytesRecived);
//                return;
//            }

//            LoggingManager.GetLogger().LogInformation("Skipped Data message handling because connection is unknown.");
//        }

//        private void OnDisconnected(INetworkPoint networkPoint)
//        {
//            ConnectionStatus = NetworkConnectionStatus.Disconnected;
//            if (serverConnection is not null && serverConnection.Item1.Equals(networkPoint))
//            {
//                serverConnection = null;
//            }
//        }

//        public void SendMessage<IMessage>(IMessage datagram) where IMessage : INetworkMessage
//        {
//            if (serverConnection is not null)
//            {
//                if (!ConnectionStatus.Equals(NetworkConnectionStatus.Connected))
//                {
//                    LoggingManager.GetLogger().LogInformation("Send when not connected to a server");
//                    return;
//                }

//                serverConnection.Item2.DatagramSending(datagram);
//                return;
//            }

//            LoggingManager.GetLogger().LogInformation("Send with no connection");
//        }

//        public virtual async void ConnectAsync(string connectionString, INetworkMessage datagram)
//        {
//            await Task.Factory.StartNew(() =>
//            {
//                LoggingManager.GetLogger().LogInformation($"Client connect to: {connectionString}");
//                ConnectionStatus = NetworkConnectionStatus.Connecting;
//                Connection.SendMessageToConnection(connectionString, datagram is null ? null : datagram.GetBytes());
//            });
//        }
//        public virtual void ConnectAsync(string connectionString) => ConnectAsync(connectionString, null);

//        public virtual void Disconnect()
//        {
//            ConnectionStatus = NetworkConnectionStatus.Disconnected;
//            if (serverConnection is not null)
//            {
//                serverConnection.Item2.Disconnect();
//            }
//        }


//        public NetworkHandler NetworkHandler { get; } = new NetworkHandler();
//        public NetworkChannels NetworkMiddlewares { get; } = new NetworkChannels();

//        public NetworkConnectionStatus ConnectionStatus { get; protected set; }
//        protected Tuple<INetworkPoint, NetworkIdentity> serverConnection;
//    }
//}
