﻿//using System;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.v2.Components;
//using Vodji.Network.v2.Connections;
//using Vodji.Network.v2.Connections.Interfaces;
//using Vodji.Shared.Utils;

//namespace Vodji.Network.v2
//{
//    public class NetworkServer<T> where T : INetworkConnection
//    {
//        public int MaxConnections { get; set; }
//        public T Connection { get; }

//        public NetworkServer()
//        {
//            Connection = Activator.CreateInstance<T>();
//            {
//                Connection.EventClientConnected.AddListener(OnConnected);
//                Connection.EventClientDisconnected.AddListener(OnDisconnected);
//                Connection.EventRecivedDatagram.AddListener(OnDataReceived);
//            }
//        }

//        public void SendMessageToAll<IMessage>(IMessage networkMessage) where IMessage : INetworkMessage
//        {
//            foreach(var networkIdentity in connections.KeysT2)
//            {
//                networkIdentity.DatagramSending(networkMessage);
//            }
//        }

//        private void OnDataReceived(INetworkPoint networkPoint, byte[] bytesRecived)
//        {
//            if (connections.TryGetValue(networkPoint, out var networkIdentity))
//            {
//                networkIdentity.DatagramReceived(bytesRecived);
//            }
//            else
//            {
//                LoggingManager.GetLogger().LogWarning("Unknown connection:" + networkPoint);
//            }
//        }

//        private void OnDisconnected(INetworkPoint networkPoint)
//        {
//            if (connections.TryGetValue(networkPoint, out var networkIdentity))
//            {
//                LoggingManager.GetLogger().LogInformation("Disconnect client: " + networkPoint);
//                connections.Remove(networkPoint);
//                networkIdentity.Disconnect();
//            }
//        }

//        private void OnConnected(INetworkPoint networkPoint)
//        {
//            LoggingManager.Logger.LogTrace($"Client connected: {networkPoint}");

//            if (connections.Contains(networkPoint))
//            {
//                LoggingManager.GetLogger().LogInformation($"[Kicked] Connection {networkPoint} already in use.");
//                Connection.Disconnect(networkPoint);
//                connections.Remove(networkPoint);
//                return;
//            }

//            if(connections.Count > MaxConnections)
//            {
//                LoggingManager.GetLogger().LogInformation($"[Kicked] Server is full. Connection {networkPoint}");
//                Connection.Disconnect(networkPoint);
//                return;
//            }

//            LoggingManager.GetLogger().LogInformation($"Server accepted client: {networkPoint}");
//            var networkIdentity = new NetworkIdentity(networkPoint);
//            {
//                networkIdentity.SetMiddleware(NetworkMiddlewares);
//                networkIdentity.SetHandler(NetworkHandler);
//                connections.Add(networkPoint, networkIdentity);
//            };
//        }

//        public NetworkHandler NetworkHandler { get; } = new NetworkHandler();
//        public NetworkChannels NetworkMiddlewares { get; } = new NetworkChannels();
//        protected DictionaryPair<INetworkPoint, NetworkIdentity> connections = new DictionaryPair<INetworkPoint, NetworkIdentity>();
//    }
//}
