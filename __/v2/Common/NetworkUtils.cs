﻿using System;
using System.Linq;
using System.Net;

namespace Vodji.Network.v2.Common
{
    public static class NetworkUtils
    {
        public static IPEndPoint Parse(string connectionHost, int defaultPort = default)
        {
            if (string.IsNullOrEmpty(connectionHost) || connectionHost.Trim().Length is 0)
                throw new ArgumentException("Endpoint descriptor may not be empty.");

            if (defaultPort != 0 && (defaultPort < IPEndPoint.MinPort || defaultPort > IPEndPoint.MaxPort))
                throw new ArgumentException(string.Format("Invalid default port '{0}'", defaultPort));

            string[] hostParsed = connectionHost.Split(new char[] { ':' });
            IPAddress ipAddress;
            //check if we have an IPv6 or ports
            if (hostParsed.Length <= 2) // ipv4 or hostname
            {
                //no port is specified, default
                if (hostParsed.Length != 1) defaultPort = GetPort(hostParsed[1]);

                //try to use the address as IPv4, otherwise get hostname
                if (!IPAddress.TryParse(hostParsed[0], out ipAddress)) ipAddress = GetIPFromHost(hostParsed[0]);
            }
            else if (hostParsed.Length > 2) //ipv6
            {
                //could [a:b:c]:d
                if (hostParsed[0].StartsWith("[") && hostParsed[hostParsed.Length - 2].EndsWith("]"))
                {
                    string ipAddressString = string.Join(":", hostParsed.Take(hostParsed.Length - 1).ToArray());
                    ipAddress = IPAddress.Parse(ipAddressString);
                    defaultPort = GetPort(hostParsed[hostParsed.Length - 1]);
                }
                else //[a:b:c] or a:b:c
                {
                    ipAddress = IPAddress.Parse(connectionHost);
                }
            }
            else
            {
                throw new FormatException(string.Format("Invalid endpoint ipaddress '{0}'", connectionHost));
            }

            if (defaultPort == default)
                throw new ArgumentException(string.Format("No port specified: '{0}'", connectionHost));

            return new IPEndPoint(ipAddress, defaultPort);
        }

        public static int GetPort(string hostAddress)
        {
            if (!int.TryParse(hostAddress, out var port) || port < IPEndPoint.MinPort || port > IPEndPoint.MaxPort)
                throw new FormatException(string.Format("Invalid end point port '{0}'", hostAddress));

            return port;
        }
        public static IPAddress GetIPFromHost(string hostName)
        {
            var iPAddresses = Dns.GetHostAddresses(hostName);

            if (iPAddresses is null || iPAddresses.Length is 0)
                throw new ArgumentException(string.Format("Host not found: {0}", hostName));

            return iPAddresses[0];
        }
    }
}
