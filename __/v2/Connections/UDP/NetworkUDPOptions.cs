﻿//using System.Net;
//using Microsoft.Extensions.Logging;

//namespace Vodji.Network.v2.Connections.UDP
//{
//    public class NetworkUDPOptions
//    {
//        private IPAddress _address = IPAddress.Loopback;
//        public IPAddress Address
//        {
//            get => _address;
//            set { if (!IsLocked()) _address = value; }
//        }

//        private int _port = 0;
//        public int Port
//        {
//            get => _port;
//            set { if (!IsLocked()) _port = value; }
//        }

//        private bool _useReuseAdress = false;
//        public bool UseReuseAdress
//        {
//            get => _useReuseAdress;
//            set { if (!IsLocked()) _useReuseAdress = value; }
//        }

//        private bool _useExclusiveAddress = true;
//        public bool UseExclusiveAddress
//        {
//            get => _useExclusiveAddress;
//            set { if (!IsLocked()) _useExclusiveAddress = value; }
//        }

//        private bool _useDualMode = true;
//        public bool UseDualMode
//        {
//            get => _useDualMode;
//            set { if (!IsLocked()) _useDualMode = value; }
//        }

//        private int _receiveBufferSize = 8192;
//        public int ReceiveBufferSize
//        {
//            get => _receiveBufferSize;
//            set { if (!IsLocked()) _receiveBufferSize = value; }
//        }

//        private int _sendBufferSize = 8192;
//        public int SendBufferSize
//        {
//            get => _sendBufferSize;
//            set { if (!IsLocked()) _sendBufferSize = value; }
//        }

//        /// <summary>
//        /// Allow set every time
//        /// </summary>
//        public int Timeout { get; set; } = 60;

//        public bool IsLocked()
//        {
//            if (!_lock) return _lock;
//            LoggingManager.GetLogger().LogWarning("Вы не можете изменять конфигурацию сети после инициализации сети.");
//            //LoggingManager.GetLogger().LogWarning("You cannot change the network configuration after network initialization.");
//            return _lock;
//        }
//        public void Lock() => _lock = true;
//        private bool _lock;
//    }
//}
