﻿//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Vodji.Network.Data.Interface;

//namespace Vodji.Network.v2.Components
//{
//    public interface INetworkMiddleware
//    {
//        public void OnDatagramReader(INetworkReader networkReader);
//        public void OnDatagramWriter(INetworkWriter networkWriter, INetworkMessage networkMessage);
//    }

//    public class NetworkChannels
//    {
//        public void AddMiddleware<T>() where T : INetworkMiddleware => middlewares.Add(Activator.CreateInstance<T>());

//        public async Task InvokeReaderAsync(INetworkReader networkReader)
//        {
//            await Task.Factory.StartNew(() =>
//            {
//                foreach(var middleware in middlewares)
//                {
//                    middleware.OnDatagramReader(networkReader);
//                }
//            });
//        }

//        public async Task InvokeWriterAsync(INetworkWriter networkWriter, INetworkMessage networkMessage)
//        {
//            await Task.Factory.StartNew(() =>
//            {
//                foreach (var middleware in middlewares)
//                {
//                    middleware.OnDatagramWriter(networkWriter, networkMessage);
//                }
//            });
//        }
//        protected List<INetworkMiddleware> middlewares = new List<INetworkMiddleware>();
//    }
//}
