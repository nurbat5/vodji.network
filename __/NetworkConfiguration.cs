﻿//using System;
//using System.Net;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;

//namespace Vodji.Network
//{
//    public class NetworkConfiguration : ICloneable
//    {
//        public NetworkConfiguration()
//        {
//            SelfAddress = IPAddress.Any;
//            Port = 0;
//            ReceiveBufferSize = 8192;
//            SendBufferSize = 8192;
//            DualMode = false;
//            MaximumConnections = 500;
//        }

//        // Блокировка конфигурации
//        public bool IsLock()
//        {
//            if (_isLocked)
//            {
//                // Вы не можете изменять конфигурацию сети после инициализации сети.
//                LoggingManager.GetLogger().LogWarning("You cannot change the network configuration after network initialization.");
//            }
//            return _isLocked;
//        }
//        public void Lock() => _isLocked = true;
//        private bool _isLocked;

//        /// <summary>
//        /// {SelfAddress}:Port
//        /// </summary>
//        public IPAddress SelfAddress
//        {
//            get => _selfAdress;
//            set
//            {
//                if (IsLock()) return;
//                _selfAdress = value;
//            }
//        }
//        private IPAddress _selfAdress;

//        /// <summary>
//        /// IPAdress:{Port}
//        /// </summary>
//        public int Port
//        {
//            get => _port;
//            set
//            {
//                if (IsLock()) return;
//                _port = value;
//            }
//        }
//        private int _port;

//        /// <summary>
//        /// Максимальный получаемый размер сообщения
//        /// </summary>
//        public int ReceiveBufferSize
//        {
//            get => _receiveBufferSize;
//            set
//            {
//                if(IsLock()) return;
//                _receiveBufferSize = value;
//            }
//        }
//        private int _receiveBufferSize;

//        /// <summary>
//        /// Максимальный отправляемые размер сообщения
//        /// </summary>
//        public int SendBufferSize
//        {
//            get => _sendBufferSize;
//            set
//            {
//                if(IsLock()) return;
//                _sendBufferSize = value;
//            }
//        }
//        private int _sendBufferSize;

//        /// <summary>
//        /// Является ли соеденение с двойным режимом, используемым для IPv4 и IPv6
//        /// </summary>
//        public bool DualMode
//        {
//            get => _dualMode;
//            set
//            {
//                if (IsLock()) return;
//                _dualMode = value;
//            }
//        }
//        private bool _dualMode;

//        /// <summary>
//        /// Максимальное количество подключений
//        /// </summary>
//        public int MaximumConnections
//        {
//            get => _maximumConnections;
//            set
//            {
//                if (IsLock()) return;
//                _maximumConnections = value;
//            }
//        }
//        private int _maximumConnections;

//        //private int _windowSize;
//        //public int WindowSize
//        //{
//        //    get => _windowSize;
//        //    set
//        //    {
//        //        if (IsLock()) return;
//        //        _windowSize = value;
//        //    }
//        //}


//        //private float _connectionTimeout;
//        //public float ConnectionTimeout
//        //{
//        //    get => _connectionTimeout;
//        //    set
//        //    {
//        //        if (value < PingInterval)
//        //        {
//        //            LogManager.GetLogger().LogWarning("Connection timeout cannot be lower than ping interval!");
//        //            return;
//        //        }
//        //        _connectionTimeout = value;
//        //    }
//        //}

//        //public float PingInterval { get; set; }                      = 4;
//        //public bool AcceptIncomingConnections { get; set; }          = false;
//        //public bool DropAboveMTU { get; set; }                       = false;

//        //public NetworkConfiguration()
//        //{
//        //    _appIdentifier = ShortGuid.NewGuid();
//        //    _disabledTypes = new HashSet<Type>();
//        //    _selfAdress = IPAddress.Any;
//        //    _port = 0;
//        //    _receiveBufferSize = 131071;
//        //    _sendBufferSize = 131071;
//        //    _dualMode = false;
//        //    _windowSize = 64;
//        //}

//        //private string _appIdentifier;






//        //public const int defaultMTU = 1400;
//        //public const string errorMSG = "Вы не можете модифицировать, после инициализации NetworkIdentity";

//        //// public NetworkUnreliableSize UnreliableSize { get; set; }    = NetworkUnreliableSize.IgnoreMTU;
//        //public bool AutoFlushSendQueue { get; set; }                 = true;
//        //public int DefaultOutgoingMessageCapacity { get; set; }      = 16;
//        //public int ExpandMTUFailAttempts { get; set; }               = 5;
//        //public float ResendHandshakeInterval { get; set; }           = 3;
//        //public float ExpandMTUFrequency { get; set; }                = 2;
//        //public float SimulatedLoss { get; set; }                     = 0;
//        //public float SimulatedMinimumLatency { get; set; }           = 0;
//        //public float SimulatedRandomLatency { get; set; }            = 0;
//        //public float SimulatedDuplicatesChance { get; set; }         = 0;

//        //private string _nameIdentifier;

//        //private IPAddress _castAdress;

//        //private bool _useMessageRecycling;
//        //private int _recycledCacheMaxCount;
//        //internal bool _enableUPnP;
//        //private bool _suppressUnreliableUnorderedAcks;

//        //private int _maximumHandshakeAttempts;

//        //// MTU
//        //private int _maximumTransmissionUnit;
//        //private bool _autoExpandMTU;

//        //public NetworkConfiguration(string identifier)
//        //{
//        //    if (string.IsNullOrEmpty(identifier))
//        //    {
//        //        LogManager.GetLogger().LogError("App identifier must be at least one character long");
//        //        return;
//        //    }

//        //    _nameIdentifier = identifier;
//        //    _castAdress = IPAddress.Broadcast;
//        //    _connectionTimeout = 25.0f;
//        //    _useMessageRecycling = true;
//        //    _recycledCacheMaxCount = 64;
//        //    _maximumHandshakeAttempts = 5;
//        //    _suppressUnreliableUnorderedAcks = false;

//        //    _maximumTransmissionUnit = defaultMTU;
//        //    _autoExpandMTU = false;
//        //}

//        //public string Identifier => _nameIdentifier;




//        //public int MaximumTransmissionUnit
//        //{
//        //    get => _maximumTransmissionUnit;
//        //    set
//        //    {
//        //        if (IsLock()) return;
//        //        if (value < 1 || value >= ((ushort.MaxValue + 1) / 8))
//        //        {
//        //            LogManager.GetLogger().LogWarning("MaximumTransmissionUnit must be between 1 and " + (((ushort.MaxValue + 1) / 8) - 1) + " bytes");
//        //            return;
//        //        }
//        //        _maximumTransmissionUnit = value;
//        //    }
//        //}
//        //public bool UseMessageRecycling
//        //{
//        //    get => _useMessageRecycling;
//        //    set
//        //    {
//        //        if (IsLock()) return;
//        //        _useMessageRecycling = value;
//        //    }
//        //}
//        //public int RecycledCacheMaxCount
//        //{
//        //    get => _recycledCacheMaxCount;
//        //    set
//        //    {
//        //        if (IsLock()) return;
//        //        _recycledCacheMaxCount = value;
//        //    }
//        //}
//        //public bool EnableUPnP
//        //{
//        //    get => _enableUPnP;
//        //    set
//        //    {
//        //        if (IsLock()) return;
//        //        _enableUPnP = value;
//        //    }
//        //}
//        //public bool SuppressUnreliableUnorderedAcks
//        //{
//        //    get => _suppressUnreliableUnorderedAcks;
//        //    set
//        //    {
//        //        if (IsLock()) return;
//        //        _suppressUnreliableUnorderedAcks = value;
//        //    }
//        //}

//        //public IPAddress BroadcastAddress
//        //{
//        //    get => _castAdress;
//        //    set
//        //    {
//        //        if (IsLock()) return;
//        //        _castAdress = value;
//        //    }
//        //}



//        //public int MaximumHandshakeAttempts
//        //{
//        //    get => _maximumHandshakeAttempts;
//        //    set
//        //    {
//        //        if (value < 1)
//        //        {
//        //            LogManager.GetLogger().LogWarning("MaximumHandshakeAttempts must be at least 1");
//        //            return;
//        //        }
//        //        _maximumHandshakeAttempts = value;
//        //    }
//        //}
//        //public bool AutoExpandMTU
//        //{
//        //    get => _autoExpandMTU;
//        //    set
//        //    {
//        //        if(IsLock()) return;
//        //        _autoExpandMTU = value;
//        //    }
//        //}
//        //public float SimulatedAverageLatency => SimulatedMinimumLatency + (SimulatedRandomLatency * 0.5f);

//        public virtual object Clone()
//        {
//            if(MemberwiseClone() is NetworkConfiguration clone)
//            {
//                clone._isLocked = false;
//                return clone;
//            }

//            return new NetworkConfiguration();
//        }
//    }
//}