﻿// using Vodji.Network.Components;
// using Vodji.Network.Other;
//
// namespace Vodji.Network.Ping.Messages
// {
//     public struct NetworkPongMessage// : INetworkMessage
//     {
//         public double clientTime;
//         public double serverTime;
//
//         public void Deserialize(INetworkReader reader)
//         {
//             reader.Read(out clientTime);
//             reader.Read(out serverTime);
//         }
//
//         public void Serialize(INetworkWriter writer)
//         {
//             writer.Write(clientTime);
//             writer.Write(serverTime);
//         }
//     }
// }