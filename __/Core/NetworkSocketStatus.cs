﻿namespace Vodji.Network.libNet
{
    public enum NetworkSocketStatus
    {
        NotRunning = 0,
        Starting = 1,
        Running = 2,
        ShutdownRequested = 3,
    }
}