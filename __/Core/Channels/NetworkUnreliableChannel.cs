﻿//using Vodji.Network.libNet.Messages;

//namespace Vodji.Network.libNet.Channels
//{
//    public class NetworkUnreliableMessage : INetworkMessage
//    {
//        public NetworkUnreliableMessage() { }
//        public NetworkUnreliableMessage(byte[] bytes) => this.bytes = bytes;
//        public byte[] bytes;
//        public void Deserialize(INetworkReader reader) => reader.ReadBytesAndSize(out bytes);
//        public void Serialize(INetworkWriter writer) => writer.WriteBytesAndSize(bytes, 0, bytes.Length);
//    }

//    public class NetworkUnreliableChannel : INetworkChannel
//    {
//        public const int CHANNEL_ID = NetworkChannel.Unreliable;
//        public NetworkPointBase NetworkPointBase { get; }
//        public _NetworkConnection NetworkConnection { get; }
//        public NetworkUnreliableChannel(NetworkPointBase networkPointBase)
//        {
//            networkPointBase.Handler.AddListener<NetworkUnreliableMessage>(OnRecivePacket);
//            NetworkPointBase = networkPointBase;
//            NetworkConnection = networkPointBase.Connection;
//        }

//        ~NetworkUnreliableChannel()
//        {
//            NetworkPointBase.Handler.RemoveListener<NetworkUnreliableMessage>();
//        }

//        public INetworkChannel Clone() => new NetworkUnreliableChannel(NetworkPointBase);

//        public void OnRecivePacket(INetworkMessage packet)
//        {
//            var msg = packet as NetworkUnreliableMessage;
//            NetworkPointBase.OnRecivedData(msg.bytes, CHANNEL_ID);
//        }

//        public void SendMessage(byte[] bytes, int sequenceChannel = 0)
//        {
//            NetworkConnection.SendPacket(bytes, NetworkPointBase.ClientEndPoint);
//        }
//    }
//}