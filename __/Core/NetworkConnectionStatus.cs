﻿namespace Vodji.Network.libNet
{
    public enum NetworkConnectionStatus
    {
        /// <summary>
        /// Отключен от сервера
        /// </summary>
        Disconnected,

        /// <summary>
        /// Данные о подключении отправлены; Ожидание ответа...
        /// </summary>
        Connecting,

        /// <summary>
        /// Данные на подключение получены; Ждем подтверждение от аунтификации сервера
        /// </summary>
        AwaitingApproval,

        /// <summary>
        /// Соеденение со сервером установлено
        /// </summary>
        Connected,

        /// <summary>
        /// Соединение со сервером утеряно (Timeout)
        /// </summary>
        ConnectionLost,

        /// <summary>
        /// Инитилизировано безопастное отключение от сервера
        /// </summary>
        Disconnecting,
    }
}