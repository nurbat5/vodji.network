﻿//using System.IO;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Shared.Extensions;

//namespace Vodji.Network.libNet.Messages
//{
//    public abstract class NetworkMessage : INetworkMessage
//    {
//        public int LengthBytes()
//        {
//            var writer = new NetworkBuffer();
//            Serialize(writer);
//            return writer.Length;
//        }

//        public abstract void Deserialize(INetworkReader reader);
//        public abstract void Serialize(INetworkWriter writer);
//    }

//    public static class NetworkMessageExtension
//    {
//        public static int GetNetworkId<T>() => typeof(T).FullName.GetStableHashCode() & 0xFFFF;
//        public static int GetNetworkId(this INetworkMessage message) => message.GetType().FullName.GetStableHashCode() & 0xFFFF;

//        public static void Pack<T>(this T message, INetworkWriter writer) where T : INetworkMessage
//        {
//            writer.Write((ushort) message.GetNetworkId());
//            message.Serialize(writer);
//        }
        
//        public static byte[] Pack<T>(this T message) where T : INetworkMessage
//        {
//            var writer = new NetworkBuffer();
//            Pack(message, writer);
//            return writer.ToArray();
//        }

//        public static T Unpack<T>(this T message, byte[] data) where T : INetworkMessage
//        {
//            INetworkReader networkReader = new NetworkBuffer(data);
//            int msgCode = message.GetNetworkId();
            
//            int _msgCode = networkReader.Read(out ushort _);
//            if (_msgCode != msgCode)
//            {
//                LoggingManager.GetLogger().LogError("Invalid message, could not unpack " + typeof(T).FullName);
//                return message;
//            }            
//            message.Deserialize(networkReader);
//            return message;
//        }
        

//        public static bool UnpackMessage(this INetworkReader messageReader, out int msgType)
//        {
//            // read message type (varint)
//            try
//            {
//                msgType = messageReader.Read(out ushort _);
//                return true;
//            }
//            catch (EndOfStreamException ex)
//            {
//                LoggingManager.GetLogger().LogError(ex, "msgType ERROR");
//                msgType = 0;
//                return false;
//            }
//        }
//    }
    
//    public interface INetworkMessage
//    {
//        void Deserialize(INetworkReader reader);
//        void Serialize(INetworkWriter writer);
//    }
//}