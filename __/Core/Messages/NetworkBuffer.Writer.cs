﻿//using System;
//using System.IO;
//using System.Runtime.Serialization;
//using System.Runtime.Serialization.Formatters.Binary;
//using Microsoft.Extensions.Logging;
//using Vodji.Logging;
//using Vodji.Network.Utils;

//namespace Vodji.Network.libNet.Messages
//{
//    public partial class NetworkBuffer : INetworkWriter
//    {
//        public void Write(object value)
//        {
//            if (value == null)
//            {
//                Write((ushort)0);
//                return;
//            }
            
//            var stream = new MemoryStream();
//            var formatter = new BinaryFormatter();
//            try
//            {
//                formatter.Serialize(stream, value);
//            }
//            catch (SerializationException e)
//            {
//                LoggingManager.GetLogger().LogCritical(e, "Failed to serialize");
//                Write((ushort)0);
//            }
//            finally
//            {
//                Write((ushort)stream.Length);
//                WriteBytes(stream.ToArray(), 0, (ushort)stream.Length);
//                stream.Close();
//            }
//        }
//        public void Write(byte value)
//        {
//            EnsureLength(_bufferPosition + 1);
//            _buffer[_bufferPosition++] = value;
//        }
//        public void Write(ulong value)
//        {
//            EnsureLength(_bufferPosition + 8);
//            _buffer[_bufferPosition++] = (byte)value;
//            _buffer[_bufferPosition++] = (byte)(value >> 8);
//            _buffer[_bufferPosition++] = (byte)(value >> 16);
//            _buffer[_bufferPosition++] = (byte)(value >> 24);
//            _buffer[_bufferPosition++] = (byte)(value >> 32);
//            _buffer[_bufferPosition++] = (byte)(value >> 40);
//            _buffer[_bufferPosition++] = (byte)(value >> 48);
//            _buffer[_bufferPosition++] = (byte)(value >> 56);
//        }
//        public void Write(uint value)
//        {
//            EnsureLength(_bufferPosition + 4);
//            _buffer[_bufferPosition++] = (byte)value;
//            _buffer[_bufferPosition++] = (byte)(value >> 8);
//            _buffer[_bufferPosition++] = (byte)(value >> 16);
//            _buffer[_bufferPosition++] = (byte)(value >> 24);
//        }
//        public void Write(ushort value)
//        {
//            Write((byte)value);
//            Write((byte)(value >> 8));
//        }

        
//        public void Write(float value)
//        {
//            var converter = new UIntFloat
//            {
//                floatValue = value
//            };
//            Write(converter.intValue);
//        }
//        public void Write(double value)
//        {
//            var converter = new UIntDouble
//            {
//                doubleValue = value
//            };
//            Write(converter.longValue);
//        }
//        public void Write(decimal value)
//        {
//            var converter = new UIntDecimal
//            {
//                decimalValue = value
//            };
//            Write(converter.longValue1);
//            Write(converter.longValue2);
//        }

//        public void Write(string value)
//        {
//            if (value == null)
//            {
//                Write((ushort)0);
//                return;
//            }
            
//            int size = _utf8Encoding.GetBytes(value, 0, value.Length, _bufferString, 0);
//            if (size >= MaxStringLength)
//            {
//                throw new IndexOutOfRangeException("NetworkWriter.Write(string) too long: " + size + ". Limit: " + MaxStringLength);
//            }
            
//            Write(checked((ushort)(size + 1)));
//            WriteBytes(_bufferString, 0, size);
//        }
//        public void Write(int value) => Write((uint) value);
//        public void Write(long value) => Write((ulong) value);
//        public void Write(sbyte value) => Write((byte) value);
//        public void Write(char value) => Write((ushort) value);
//        public void Write(bool value) => Write((byte) (value ? 1 : 0));
//        public void Write(short value) => Write((ushort) value);
//        public void Write(Uri uri) => Write(uri.ToString());

//        protected void WritePacked(long value) => WritePacked((ulong) ((value >> 63) ^ (value << 1)));
//        protected void WritePacked(int value) => WritePacked((uint) ((value >> 31) ^ (value << 1)));
//        protected void WritePacked(uint value) => WritePacked((ulong) value);
//        protected void WritePacked(ulong value)
//        {
//            if (value <= 240)
//            {
//                Write((byte)value);
//                return;
//            }
//            if (value <= 2287)
//            {
//                Write((byte)(((value - 240) >> 8) + 241));
//                Write((byte)(value - 240));
//                return;
//            }
//            if (value <= 67823)
//            {
//                Write(249);
//                Write((byte)((value - 2288) >> 8));
//                Write((byte)(value - 2288));
//                return;
//            }
//            if (value <= 16777215)
//            {
//                Write(250);
//                Write((byte)value);
//                Write((byte)(value >> 8));
//                Write((byte)(value >> 16));
//                return;
//            }
//            if (value <= 4294967295)
//            {
//                Write(251);
//                Write((byte)value);
//                Write((byte)(value >> 8));
//                Write((byte)(value >> 16));
//                Write((byte)(value >> 24));
//                return;
//            }
//            if (value <= 1099511627775)
//            {
//                Write(252);
//                Write((byte)value);
//                Write((byte)(value >> 8));
//                Write((byte)(value >> 16));
//                Write((byte)(value >> 24));
//                Write((byte)(value >> 32));
//                return;
//            }
//            if (value <= 281474976710655)
//            {
//                Write(253);
//                Write((byte)value);
//                Write((byte)(value >> 8));
//                Write((byte)(value >> 16));
//                Write((byte)(value >> 24));
//                Write((byte)(value >> 32));
//                Write((byte)(value >> 40));
//                return;
//            }
//            if (value <= 72057594037927935)
//            {
//                Write(254);
//                Write((byte)value);
//                Write((byte)(value >> 8));
//                Write((byte)(value >> 16));
//                Write((byte)(value >> 24));
//                Write((byte)(value >> 32));
//                Write((byte)(value >> 40));
//                Write((byte)(value >> 48));
//                return;
//            }

//            // all others
//            {
//                Write(255);
//                Write((byte)value);
//                Write((byte)(value >> 8));
//                Write((byte)(value >> 16));
//                Write((byte)(value >> 24));
//                Write((byte)(value >> 32));
//                Write((byte)(value >> 40));
//                Write((byte)(value >> 48));
//                Write((byte)(value >> 56));
//            }
//        }

//        public void WriteBytes(byte[] bytes, int offset, int count)
//        {
//            EnsureLength(_bufferPosition + count);
//            Array.ConstrainedCopy(bytes, offset, _buffer, _bufferPosition, count);
//            _bufferPosition += count;
//        }
//        public void WriteBytesAndSize(byte[] buffer, int offset, int count)
//        {
//            if (buffer == null)
//            {
//                Write((ushort)0);
//                return;
//            }
//            Write(checked((ushort)(count + 1)));
//            WriteBytes(buffer, offset, count);
//        }
        
//        protected void WriteBytesAndSize(byte[] buffer) => WriteBytesAndSize(buffer, 0, buffer?.Length ?? 0);
//        protected void WriteBytesAndSizeSegment(ArraySegment<byte> buffer) => WriteBytesAndSize(buffer.Array, buffer.Offset, buffer.Count);
//    }
//}