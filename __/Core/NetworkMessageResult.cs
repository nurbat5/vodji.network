﻿namespace Vodji.Network.libNet
{
    public enum NetworkMessageResult
    {
        NotConnected = 0,
        Sent = 1,
        Queued = 2,
        Dropped = 3
    }
}