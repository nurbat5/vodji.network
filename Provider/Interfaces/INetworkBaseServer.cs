﻿using Vodji.Network.Components.Interfaces;
using Vodji.Shared.Utils;

namespace Vodji.Network.Provider.Interfaces
{
    public interface INetworkBaseServer
    {
        public int MaxConnections { get; set; }
        public float Timeout { get; set; }

        public EventCallback<INetworkPoint> EventClientConnected { get; }
        public EventCallback<INetworkPoint> EventClientDisconnected { get; }
        public EventCallback<INetworkPoint, byte[]> EventClientDataReceived { get; }

        public void Listen(INetworkAddress virtualPort);
        public bool Disconnect(INetworkPoint networkPoint);
        public void Send(INetworkPoint networkPoint, byte[] bytesMessage, int channel);
    }
}
