﻿using Vodji.Network.Components.Interfaces;

namespace Vodji.Network.Components
{
    public class NetworkMessage : INetworkMessage
    {
        public void Deserialize(INetworkStream networkStream)
        {
            foreach (var getPropertyInfo in GetType().GetProperties())
            {
                getPropertyInfo.SetValue(this, networkStream.Read(getPropertyInfo.PropertyType));
            }
        }

        public void Serialize(INetworkStream networkStream)
        {
            foreach (var getPropertyInfo in GetType().GetProperties())
            {
                networkStream.Write(getPropertyInfo.GetValue(this));
            }
        }
    }
}
