﻿using System;

namespace Vodji.Network.Components.Interfaces
{
    public interface INetworkIdentity
    {
        public INetworkPoint NetworkPoint { get; }
        public NetworkMessageHandler<INetworkIdentity> NetworkMessageHandler { get; set; }

        public bool IsTimeout(float timeout);

        //public NetworkMessageHandler messageHandler { get; }
        //public object Authenticated { get; set; }
        //public void Send<T>(T msg, int channelId = Channels.Reliable);

        public void SendMessage<T>(T msg);
        public void OnDataReceived(byte[] bytesRecived);
        //public void Update();
        public void Disconnect();
    }
}
