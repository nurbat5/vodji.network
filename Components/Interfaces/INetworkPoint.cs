﻿namespace Vodji.Network.Components.Interfaces
{
    public interface INetworkPoint
    {
        public NetworkId Id { get; }
        public object Connection { get; }
    }
}
